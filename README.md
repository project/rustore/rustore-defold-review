## RuStore Defold плагин для оценок и отзывов

### [🔗 Документация разработчика][10]

Плагин “RuStoreDefoldReview” позволяет пользователю оставить оценку и отзыв о вашем приложении в RuStore, не выходя из приложения.

Репозиторий содержит плагины “RuStoreDefoldReview” и “RuStoreDefoldCore”, а также демонстрационное приложение с примерами использования и настроек. Поддерживаются версии Defold 1.6.2+.


### Сборка примера приложения

Вы можете ознакомиться с демонстрационным приложением содержащим представление работы всех методов sdk:
- [README](review_example/README.md)
- [review_example](https://gitflic.ru/project/rustore/rustore-defold-review/file?file=review_example)


### Установка плагина в свой проект

1. Скопируйте папки _“review_example / extension_rustore_review”_ и _“review_example / extension_rustore_core”_ в корень вашего проекта.


### Пересборка плагина

Если вам необходимо изменить код библиотек плагинов, вы можете внести изменения и пересобрать подключаемые .jar файлы.

1. Откройте в вашей IDE проект Android из папки _“extension_libraries”_.

2. Внесите необходимые изменения.

3. Выполните сборку проекта командой gradle assemble.

При успешном выполнении сборки в папках _“review_example / extension_rustore_review / lib / android”_ и _“review_example / extension_rustore_core / lib / android”_ будут обновлены файлы:
- RuStoreDefoldReview.jar
- RuStoreDefoldCore.jar


### История изменений

[CHANGELOG](CHANGELOG.md)


### Условия распространения

Данное программное обеспечение, включая исходные коды, бинарные библиотеки и другие файлы распространяется под лицензией MIT. Информация о лицензировании доступна в документе [MIT-LICENSE](MIT-LICENSE.txt).


### Техническая поддержка

Дополнительная помощь и инструкции доступны на странице [rustore.ru/help/](https://www.rustore.ru/help/) и по электронной почте [support@rustore.ru](mailto:support@rustore.ru).

[10]: https://www.rustore.ru/help/sdk/reviews-ratings/defold/6-0-0
