package ru.rustore.defold.review

import android.app.Activity
import com.google.gson.Gson
import ru.rustore.defold.core.RuStoreCore
import ru.rustore.sdk.core.exception.RuStoreException
import ru.rustore.sdk.review.RuStoreReviewManager
import ru.rustore.sdk.review.RuStoreReviewManagerFactory
import ru.rustore.sdk.review.model.ReviewInfo

object RuStoreReview {
    private const val CHANNEL_REQUEST_REVIEW_FLOW_SUCCESS = "rustore_request_review_flow_success"
    private const val CHANNEL_REQUEST_REVIEW_FLOW_FAILURE = "rustore_request_review_flow_failure"
    private const val CHANNEL_LAUNCH_REVIEW_FLOW_SUCCESS = "rustore_launch_review_flow_success"
    private const val CHANNEL_LAUNCH_REVIEW_FLOW_FAILURE = "rustore_launch_review_flow_failure"
    private const val UNKNOWN_ERROR = "Unknown error"

    private lateinit var reviewManager: RuStoreReviewManager
    private var isInitialized: Boolean = false
    private var reviewInfo: ReviewInfo? = null
    private val gson = Gson()

    @JvmStatic
    fun init(activity: Activity, metricType: String) {
        if (isInitialized) return
        reviewManager = RuStoreReviewManagerFactory.create(
            context = activity.application,
            internalConfig = mapOf("type" to metricType)
        )
        isInitialized = true;
    }

    @JvmStatic
    fun requestReviewFlow() {
        if (!isInitialized) {
            val throwable = RuStoreException(UNKNOWN_ERROR)
            RuStoreCore.emitSignal(CHANNEL_REQUEST_REVIEW_FLOW_FAILURE, gson.toJson(throwable))
            return
        }

        reviewManager.requestReviewFlow()
            .addOnSuccessListener { result ->
                reviewInfo = result
                RuStoreCore.emitSignal(CHANNEL_REQUEST_REVIEW_FLOW_SUCCESS, String())
            }
            .addOnFailureListener { throwable ->
                RuStoreCore.emitSignal(CHANNEL_REQUEST_REVIEW_FLOW_FAILURE, gson.toJson(throwable))
            }
    }

    @JvmStatic
    fun launchReviewFlow() {
        if (!isInitialized) {
            val throwable = RuStoreException(UNKNOWN_ERROR)
            RuStoreCore.emitSignal(CHANNEL_LAUNCH_REVIEW_FLOW_FAILURE, gson.toJson(throwable))
            return
        }

        reviewInfo?.let {
            reviewManager.launchReviewFlow(reviewInfo = it)
                .addOnSuccessListener {
                    RuStoreCore.emitSignal(CHANNEL_LAUNCH_REVIEW_FLOW_SUCCESS, String())
                }
                .addOnFailureListener { throwable ->
                    RuStoreCore.emitSignal(CHANNEL_LAUNCH_REVIEW_FLOW_FAILURE, gson.toJson(throwable))
                }
        }
    }
}
